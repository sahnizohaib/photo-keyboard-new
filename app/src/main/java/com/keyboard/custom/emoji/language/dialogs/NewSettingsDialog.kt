package com.keyboard.custom.emoji.language.dialogs

import android.app.Dialog
import android.content.Context
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import android.view.Window

import com.keyboard.custom.emoji.language.R
import com.keyboard.custom.emoji.language.utils.MyConstants
import kotlinx.android.synthetic.main.layout_settings_dialog.*

class NewSettingsDialog(private val mContext: Context) : Dialog(mContext), View.OnClickListener {

    private var shouldVibrate = false
    private var shouldSound = false
    private var shouldKey = false
    private lateinit var sharedPref: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setCancelable(false)
        window!!.requestFeature(Window.FEATURE_NO_TITLE)
        window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        setContentView(R.layout.layout_settings_dialog)
        initViews()
    }

    private fun initViews() {
        sharedPref = mContext.getSharedPreferences(MyConstants.PREFS_NAME, Context.MODE_PRIVATE)
        shouldVibrate = sharedPref.getBoolean(MyConstants.KEYBOARD_VIBRATE_KEY, false)
        shouldSound = sharedPref.getBoolean(MyConstants.KEYBOARD_SOUND_KEY, false)
        shouldKey = sharedPref.getBoolean(MyConstants.KEYBOARD_PREVIEW_KEY, false)
        checkbox.isChecked = shouldVibrate
        checkbox2.isChecked = shouldKey
        checkbox3.isChecked = shouldSound
        vibrateLayout.setOnClickListener(this)
        previewLayout.setOnClickListener(this)
        soundLayout.setOnClickListener(this)
        cardViewYes.setOnClickListener(this)
    }

    override fun onClick(p0: View?) {
        when (p0?.id) {
            R.id.cardViewYes -> {
                dismiss()
            }
            R.id.vibrateLayout -> {
                if (!shouldVibrate) {
                    shouldVibrate = true
                    checkbox.isChecked = true
                    sharedPref.edit().putBoolean(MyConstants.KEYBOARD_VIBRATE_KEY, true).apply()
                } else {
                    shouldVibrate = false
                    checkbox.isChecked = false
                    sharedPref.edit().putBoolean(MyConstants.KEYBOARD_VIBRATE_KEY, false).apply()
                }
            }
            R.id.previewLayout -> {
                if (!shouldKey) {
                    shouldKey = true
                    checkbox2.isChecked = true
                    sharedPref.edit().putBoolean(MyConstants.KEYBOARD_PREVIEW_KEY, true).apply()
                } else {
                    shouldKey = false
                    checkbox2.isChecked = false
                    sharedPref.edit().putBoolean(MyConstants.KEYBOARD_PREVIEW_KEY, false).apply()
                }
            }
            R.id.soundLayout -> {
                if (!shouldSound) {
                    shouldSound = true
                    checkbox3.isChecked = true
                    sharedPref.edit().putBoolean(MyConstants.KEYBOARD_SOUND_KEY, true).apply()
                } else {
                    shouldSound = false
                    checkbox3.isChecked = false
                    sharedPref.edit().putBoolean(MyConstants.KEYBOARD_SOUND_KEY, false).apply()
                }
            }
        }
    }
}
