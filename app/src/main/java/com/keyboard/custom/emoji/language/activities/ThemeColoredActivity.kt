package com.keyboard.custom.emoji.language.activities

import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.ads.AdView
import com.keyboard.custom.emoji.language.R
import com.keyboard.custom.emoji.language.adapters.MyAppCustomizationAdapter
import com.keyboard.custom.emoji.language.interfaces.CustomTypeAdapterCallback
import com.keyboard.custom.emoji.language.models.CustomTypeModel
import com.keyboard.custom.emoji.language.services.PhotoKeyboardService
import com.keyboard.custom.emoji.language.utils.MyAdsHelper
import com.keyboard.custom.emoji.language.utils.MyConstants
import kotlinx.android.synthetic.main.activity_wallpaper_customization.*

class ThemeColoredActivity : AppCompatActivity() {

    private var list = ArrayList<CustomTypeModel>()
    private lateinit var sharedPreferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_colored_customization)
        initViews()
    }

    private fun initViews() {
        val mAdView = findViewById<AdView>(R.id.adView)
        MyAdsHelper.loadBannerAd(mAdView, this)
        sharedPreferences =
            applicationContext.getSharedPreferences(MyConstants.PREFS_NAME, Context.MODE_PRIVATE)
        initLists()
        ivBack.setOnClickListener { onBackPressed() }
        recyclerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL,false)
        val adapter = MyAppCustomizationAdapter(
            list,
            this,
            object : CustomTypeAdapterCallback {
                override fun onThemeClicked(model: CustomTypeModel?, position: Int) {
                    PhotoKeyboardService.selectedThemeIndex = (position + 30)
                    Log.d("SELECTEDINDEX", " Color " + PhotoKeyboardService.selectedThemeIndex)
                    try {
                        Toast.makeText(
                            this@ThemeColoredActivity,
                            "" + model!!.themeName + " is selected as keyboard Background",
                            Toast.LENGTH_LONG
                        ).show()
                        sharedPreferences.edit()
                            .putInt(MyConstants.THEME_KEYBOARD_DRAWABLE, model.themeBackground)
                            .apply()
                        sharedPreferences.edit()
                            .putInt(
                                MyConstants.THEME_KEYBOARD_INDEX,
                                PhotoKeyboardService.selectedThemeIndex
                            )
                            .apply()
                        sharedPreferences.edit().putString(
                            MyConstants.THEME_KEYBOARD_MODE,
                            MyConstants.THEME_KEYBOARD_MODE_DRAWABLE
                        )
                            .apply()
                        onBackPressed()
                    } catch (e: Exception) {
                    }
                }
            })
        recyclerView.adapter = adapter
    }


    private fun initLists() {

        list.add(CustomTypeModel("Theme Blue Violet", R.color.BlueViolet))
        list.add(CustomTypeModel("Theme Brown", R.color.Brown))
        list.add(CustomTypeModel("Theme Chartreuse", R.color.Chartreuse))
        list.add(CustomTypeModel("Theme Chocolate", R.color.Chocolate))
        list.add(CustomTypeModel("Theme Cyan", R.color.Cyan))
        list.add(CustomTypeModel("Theme Dark Blue", R.color.DarkBlue))
        list.add(CustomTypeModel("Theme AliceBlue", R.color.AliceBlue))
        list.add(CustomTypeModel("Theme Antique White", R.color.AntiqueWhite))
        list.add(CustomTypeModel("Theme Aqua", R.color.Aqua))
        list.add(CustomTypeModel("Theme Azure", R.color.Azure))
        list.add(CustomTypeModel("Theme Black", R.color.Black))
        list.add(CustomTypeModel("Theme Blanched Almond", R.color.BlanchedAlmond))
        list.add(CustomTypeModel("Theme Blue", R.color.Blue))
        list.add(CustomTypeModel("Theme Dark SeaGreen", R.color.DarkSeaGreen))
        list.add(CustomTypeModel("Theme Dark Violet", R.color.DarkViolet))
        list.add(CustomTypeModel("Theme Deep Pink", R.color.DeepPink))
        list.add(CustomTypeModel("Theme Deep Sky Blue", R.color.DeepSkyBlue))
        list.add(CustomTypeModel("Theme Ghost White", R.color.GhostWhite))
        list.add(CustomTypeModel("Theme Gold", R.color.Gold))
        list.add(CustomTypeModel("Theme Golden Rod", R.color.GoldenRod))
        list.add(CustomTypeModel("Theme Gray", R.color.Gray))
        list.add(CustomTypeModel("Theme Green", R.color.Green))
        list.add(CustomTypeModel("Theme Honey Dew", R.color.HoneyDew))
        list.add(CustomTypeModel("Theme Indigo", R.color.Indigo))
        list.add(CustomTypeModel("Theme Dark Cyan", R.color.DarkCyan))
        list.add(CustomTypeModel("Theme Dark Golden Rod", R.color.DarkGoldenRod))
        list.add(CustomTypeModel("Theme Dark Gray", R.color.DarkGray))
        list.add(CustomTypeModel("Theme Dark Green", R.color.DarkGreen))
        list.add(CustomTypeModel("Theme Dark orange", R.color.Darkorange))
        list.add(CustomTypeModel("Theme Dark Red", R.color.DarkRed))
        list.add(CustomTypeModel("Theme Dark Salmon", R.color.DarkSalmon))
        list.add(CustomTypeModel("Theme Lawn Green", R.color.LawnGreen))
        list.add(CustomTypeModel("Theme Navy", R.color.Navy))
        list.add(CustomTypeModel("Theme Orange", R.color.Orange))
        list.add(CustomTypeModel("Theme Orange Red", R.color.OrangeRed))
        list.add(CustomTypeModel("Theme Peru", R.color.Peru))
        list.add(CustomTypeModel("Theme Lemon Chiffon", R.color.LemonChiffon))
        list.add(CustomTypeModel("Theme Light Blue", R.color.LightBlue))
        list.add(CustomTypeModel("Theme Light Green", R.color.LightGreen))
        list.add(CustomTypeModel("Theme Light Grey", R.color.LightGrey))
        list.add(CustomTypeModel("Theme Light Pink", R.color.LightPink))
        list.add(CustomTypeModel("Theme Light Steel Blue", R.color.LightSteelBlue))
        list.add(CustomTypeModel("Theme Maroon", R.color.Maroon))
        list.add(CustomTypeModel("Theme Pink", R.color.Pink))
        list.add(CustomTypeModel("Theme Purple", R.color.Purple))
        list.add(CustomTypeModel("Theme Red", R.color.Red))
        list.add(CustomTypeModel("Theme Sky Blue", R.color.SkyBlue))
        list.add(CustomTypeModel("Theme Snow", R.color.Snow))
        list.add(CustomTypeModel("Theme White Smoke", R.color.WhiteSmoke))
        list.add(CustomTypeModel("Theme Yellow", R.color.Yellow))
    }
}
