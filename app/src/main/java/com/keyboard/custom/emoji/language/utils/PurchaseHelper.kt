package com.keyboard.custom.emoji.language.utils

import android.app.Activity
import android.content.Context
import android.widget.Toast

import com.anjlab.android.iab.v3.BillingProcessor
import com.anjlab.android.iab.v3.TransactionDetails
import com.keyboard.custom.emoji.language.R


class PurchaseHelper(private val context: Context) : BillingProcessor.IBillingHandler {
    private val billingProcessor: BillingProcessor =
        BillingProcessor(context, context.getString(R.string.billing_key), this)

    init {
        billingProcessor.initialize()
    }

    fun shouldGiveAds(): Boolean {
        return !billingProcessor.isPurchased("remove_app_ads")
    }


    override fun onProductPurchased(productId: String, details: TransactionDetails?) {
        Toast.makeText(context, "Please Restart App !", Toast.LENGTH_LONG).show()
    }

    override fun onPurchaseHistoryRestored() {}

    override fun onBillingError(errorCode: Int, error: Throwable?) {}

    override fun onBillingInitialized() {}
}
