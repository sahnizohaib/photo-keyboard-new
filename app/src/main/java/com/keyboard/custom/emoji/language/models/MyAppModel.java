package com.keyboard.custom.emoji.language.models;

public class MyAppModel {
    String appItemName;
    int appItemIcon;
    String appItemDetails;

    public MyAppModel(String appItemName, int appItemIcon, String appItemDetails) {
        this.appItemName = appItemName;
        this.appItemIcon = appItemIcon;
        this.appItemDetails = appItemDetails;
    }

    public MyAppModel(String itemName, int itemIcon) {
        this.appItemName = itemName;
        this.appItemIcon = itemIcon;
    }

    public String getAppItemName() {
        return appItemName;
    }

    public void setAppItemName(String appItemName) {
        this.appItemName = appItemName;
    }

    public int getAppItemIcon() {
        return appItemIcon;
    }

    public void setAppItemIcon(int appItemIcon) {
        this.appItemIcon = appItemIcon;
    }

    public String getAppItemDetails() {
        return appItemDetails;
    }

    public void setAppItemDetails(String appItemDetails) {
        this.appItemDetails = appItemDetails;
    }
}
