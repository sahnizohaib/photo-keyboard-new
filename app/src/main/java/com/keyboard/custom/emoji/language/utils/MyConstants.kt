package com.keyboard.custom.emoji.language.utils

import com.keyboard.custom.emoji.language.R
import com.keyboard.custom.emoji.language.models.CustomTypeModel

class MyConstants {
    companion object {
        val PREFS_NAME = "PREFS_NAME"
        var list = ArrayList<CustomTypeModel>()
        val THEME_KEYBOARD_DRAWABLE = "THEME_KEYBOARD_DRAWABLE"
        val THEME_KEYBOARD_GALLERY = "THEME_KEYBOARD_GALLERY"


        val THEME_KEYBOARD_INDEX = "THEME_KEYBOARD_INDEX"

        val THEME_KEYBOARD_MODE = "THEME_KEYBOARD_MODE"
        val THEME_KEYBOARD_MODE_DRAWABLE = "THEME_KEYBOARD_MODE_DRAWABLE"
        val THEME_KEYBOARD_MODE_GALLERY = "THEME_KEYBOARD_MODE_GALLERY"

        var KEYBOARD_VIBRATE_KEY = "KEYBOARD_VIBRATE_KEY"
        var KEYBOARD_PREVIEW_KEY = "KEYBOARD_PREVIEW_KEY"
        var KEYBOARD_SOUND_KEY = "KEYBOARD_SOUND_KEY"

        fun initLists() {

            list.add(CustomTypeModel("Theme Sea view", R.drawable.bg1))
            list.add(CustomTypeModel("Theme Snow View", R.drawable.bg2))
            list.add(CustomTypeModel("Theme Blurred Fish", R.drawable.bg3))
            list.add(CustomTypeModel("Theme Yellow Fish", R.drawable.bg4))
            list.add(CustomTypeModel("Theme Gradient Orange", R.drawable.bg5))
            list.add(CustomTypeModel("Theme Colorful World", R.drawable.bg6))
            list.add(CustomTypeModel("Theme Sea Sight", R.drawable.bg7))
            list.add(CustomTypeModel("Theme Sunset", R.drawable.bg8))
            list.add(CustomTypeModel("Theme Pink Hearts", R.drawable.bg9))
            list.add(CustomTypeModel("Theme River Side", R.drawable.bg10))
            list.add(CustomTypeModel("Theme Shades", R.drawable.bg11))
            list.add(CustomTypeModel("Theme Blocks", R.drawable.bg12))
            list.add(CustomTypeModel("Theme Circular Lines", R.drawable.bg13))
            list.add(CustomTypeModel("Theme Drops", R.drawable.bg14))
            list.add(CustomTypeModel("Theme Stars", R.drawable.bg15))
            list.add(CustomTypeModel("Theme Shapes", R.drawable.bg16))
            list.add(CustomTypeModel("Theme Flowers", R.drawable.bg17))
            list.add(CustomTypeModel("Theme Hearts", R.drawable.bg18))
            list.add(CustomTypeModel("Theme Lights", R.drawable.bg19))
            list.add(CustomTypeModel("Theme Tiles", R.drawable.bg20))
            list.add(CustomTypeModel("Theme Mountains", R.drawable.bg21))
            list.add(CustomTypeModel("Theme Digital", R.drawable.bg22))
            list.add(CustomTypeModel("Theme Leaves", R.drawable.bg23))
            list.add(CustomTypeModel("Theme Mountain View", R.drawable.bg24))
            list.add(CustomTypeModel("Theme Tiger", R.drawable.bg25))
            list.add(CustomTypeModel("Theme Lion", R.drawable.bg26))
            list.add(CustomTypeModel("Theme Yellow Leaves", R.drawable.bg27))
            list.add(CustomTypeModel("Theme White Lion", R.drawable.bg28))
            list.add(CustomTypeModel("Theme Night Street", R.drawable.bg29))
            list.add(CustomTypeModel("Theme Plant View", R.drawable.bg30))
            list.add(CustomTypeModel("Theme Sand", R.drawable.bg31))
            list.add(CustomTypeModel("Theme Sunrise", R.drawable.bg32))
            list.add(CustomTypeModel("Theme Desert", R.drawable.bg33))
            list.add(CustomTypeModel("Theme Camels & Mountains", R.drawable.bg34))
            list.add(CustomTypeModel("Theme Mountain Side", R.drawable.bg35))
            list.add(CustomTypeModel("Theme Strawberry Plate", R.drawable.bg36))
            list.add(CustomTypeModel("Theme Fresh Strawberry", R.drawable.bg37))
            list.add(CustomTypeModel("Theme Fruits", R.drawable.bg38))
            list.add(CustomTypeModel("Theme Pear", R.drawable.bg39))
            list.add(CustomTypeModel("Theme Lemon", R.drawable.bg40))
            list.add(CustomTypeModel("Theme Black Cat", R.drawable.bg41))
            list.add(CustomTypeModel("Theme Sleeping Cat", R.drawable.bg42))
            list.add(CustomTypeModel("Theme Black Cat 2", R.drawable.bg44))
            list.add(CustomTypeModel("Theme Naughty Cat", R.drawable.bg44))
            list.add(CustomTypeModel("Theme Black Butterfly", R.drawable.bg45))
            list.add(CustomTypeModel("Theme Yellow ", R.drawable.bg46))
            list.add(CustomTypeModel("Theme Group of Butterfly", R.drawable.bg47))
            list.add(CustomTypeModel("Theme Bonfire", R.drawable.bg48))
            list.add(CustomTypeModel("Theme Fire", R.drawable.bg49))
            list.add(CustomTypeModel("Theme Black & White Cat", R.drawable.bg50))
            list.add(
                CustomTypeModel(
                    "Theme Blue Violet",
                    R.color.BlueViolet
                )
            )
            list.add(
                CustomTypeModel(
                    "Theme Brown",
                    R.color.Brown
                )
            )
            list.add(
                CustomTypeModel(
                    "Theme Chartreuse",
                    R.color.Chartreuse
                )
            )
            list.add(
                CustomTypeModel(
                    "Theme Chocolate",
                    R.color.Chocolate
                )
            )
            list.add(
                CustomTypeModel(
                    "Theme Cyan",
                    R.color.Cyan
                )
            )
            list.add(
                CustomTypeModel(
                    "Theme Dark Blue",
                    R.color.DarkBlue
                )
            )
            list.add(
                CustomTypeModel(
                    "Theme Dark Cyan",
                    R.color.DarkCyan
                )
            )
            list.add(
                CustomTypeModel(
                    "Theme Dark Golden Rod",
                    R.color.DarkGoldenRod
                )
            )
            list.add(
                CustomTypeModel(
                    "Theme Dark Gray",
                    R.color.DarkGray
                )
            )
            list.add(
                CustomTypeModel(
                    "Theme Dark Green",
                    R.color.DarkGreen
                )
            )
            list.add(
                CustomTypeModel(
                    "Theme Dark orange",
                    R.color.Darkorange
                )
            )
            list.add(
                CustomTypeModel(
                    "Theme Dark Red",
                    R.color.DarkRed
                )
            )
            list.add(
                CustomTypeModel(
                    "Theme Dark Salmon",
                    R.color.DarkSalmon
                )
            )
            list.add(
                CustomTypeModel(
                    "Theme Dark SeaGreen",
                    R.color.DarkSeaGreen
                )
            )
            list.add(
                CustomTypeModel(
                    "Theme AliceBlue",
                    R.color.AliceBlue
                )
            )
            list.add(
                CustomTypeModel(
                    "Theme Antique White",
                    R.color.AntiqueWhite
                )
            )
            list.add(
                CustomTypeModel(
                    "Theme Aqua",
                    R.color.Aqua
                )
            )
            list.add(
                CustomTypeModel(
                    "Theme Azure",
                    R.color.Azure
                )
            )
            list.add(
                CustomTypeModel(
                    "Theme Black",
                    R.color.Black
                )
            )
            list.add(
                CustomTypeModel(
                    "Theme Blanched Almond",
                    R.color.BlanchedAlmond
                )
            )
            list.add(
                CustomTypeModel(
                    "Theme Blue",
                    R.color.Blue
                )
            )
            list.add(
                CustomTypeModel(
                    "Theme Dark Violet",
                    R.color.DarkViolet
                )
            )
            list.add(
                CustomTypeModel(
                    "Theme Deep Pink",
                    R.color.DeepPink
                )
            )
            list.add(
                CustomTypeModel(
                    "Theme Deep Sky Blue",
                    R.color.DeepSkyBlue
                )
            )
            list.add(
                CustomTypeModel(
                    "Theme Ghost White",
                    R.color.GhostWhite
                )
            )
            list.add(
                CustomTypeModel(
                    "Theme Gold",
                    R.color.Gold
                )
            )
            list.add(
                CustomTypeModel(
                    "Theme Golden Rod",
                    R.color.GoldenRod
                )
            )
            list.add(
                CustomTypeModel(
                    "Theme Gray",
                    R.color.Gray
                )
            )
            list.add(
                CustomTypeModel(
                    "Theme Green",
                    R.color.Green
                )
            )
            list.add(
                CustomTypeModel(
                    "Theme Honey Dew",
                    R.color.HoneyDew
                )
            )
            list.add(
                CustomTypeModel(
                    "Theme Indigo",
                    R.color.Indigo
                )
            )
            list.add(
                CustomTypeModel(
                    "Theme Lawn Green",
                    R.color.LawnGreen
                )
            )
            list.add(
                CustomTypeModel(
                    "Theme Lemon Chiffon",
                    R.color.LemonChiffon
                )
            )
            list.add(
                CustomTypeModel(
                    "Theme Light Blue",
                    R.color.LightBlue
                )
            )
            list.add(
                CustomTypeModel(
                    "Theme Light Green",
                    R.color.LightGreen
                )
            )
            list.add(
                CustomTypeModel(
                    "Theme Light Grey",
                    R.color.LightGrey
                )
            )
            list.add(
                CustomTypeModel(
                    "Theme Light Pink",
                    R.color.LightPink
                )
            )
            list.add(
                CustomTypeModel(
                    "Theme Light Steel Blue",
                    R.color.LightSteelBlue
                )
            )
            list.add(
                CustomTypeModel(
                    "Theme Maroon",
                    R.color.Maroon
                )
            )
            list.add(
                CustomTypeModel(
                    "Theme Navy",
                    R.color.Navy
                )
            )
            list.add(
                CustomTypeModel(
                    "Theme Orange",
                    R.color.Orange
                )
            )
            list.add(
                CustomTypeModel(
                    "Theme Orange Red",
                    R.color.OrangeRed
                )
            )
            list.add(
                CustomTypeModel(
                    "Theme Peru",
                    R.color.Peru
                )
            )
            list.add(
                CustomTypeModel(
                    "Theme Pink",
                    R.color.Pink
                )
            )
            list.add(
                CustomTypeModel(
                    "Theme Purple",
                    R.color.Purple
                )
            )
            list.add(
                CustomTypeModel(
                    "Theme Sky Blue",
                    R.color.SkyBlue
                )
            )
            list.add(
                CustomTypeModel(
                    "Theme Snow",
                    R.color.Snow
                )
            )
            list.add(
                CustomTypeModel(
                    "Theme White Smoke",
                    R.color.WhiteSmoke
                )
            )
            list.add(
                CustomTypeModel(
                    "Theme Yellow",
                    R.color.Yellow
                )
            )
            list.add(CustomTypeModel(" Theme Red", R.color.Red))
        }
    }
}