package com.keyboard.custom.emoji.language.activities

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd
import com.keyboard.custom.emoji.language.R
import com.keyboard.custom.emoji.language.utils.PurchaseHelper

class MyAppSplashActivity : AppCompatActivity() {
    private var adMobInterstitialAd: InterstitialAd? = null
    private var isPaused = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        val billingHelper = PurchaseHelper(this)
        adMobInterstitialAd = InterstitialAd(this)
        adMobInterstitialAd?.adUnitId = getString(R.string.interstitial_splash_admob)
        if (billingHelper.shouldGiveAds()) {
            adMobInterstitialAd?.loadAd(AdRequest.Builder().build())
            adMobInterstitialAd?.adListener = object : AdListener() {
                override fun onAdLoaded() {
                    super.onAdLoaded()
                    if (adMobInterstitialAd != null && adMobInterstitialAd!!.isLoaded) {
                        adMobInterstitialAd?.show()
                        adMobInterstitialAd?.adListener = object : AdListener() {
                            override fun onAdClosed() {
                                super.onAdClosed()
                                moveToMain()
                            }
                        }
                    }
                }

                override fun onAdFailedToLoad(p0: Int) {
                    moveToMain()
                }
            }
        } else {
            Handler().postDelayed({
                moveToMain()
            }, 3000)
        }
    }

    override fun onDestroy() {
        adMobInterstitialAd = null
        super.onDestroy()
    }

    private fun moveToMain() {
        val intentToMain = Intent(this, MyAppMainActivity::class.java)
        intentToMain.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intentToMain)
        finish()
    }

    override fun onPause() {
        isPaused = true
        super.onPause()
    }

    override fun onResume() {
        super.onResume()
        if (isPaused) {
            Handler().postDelayed({ moveToMain() }, 2000)
        }
    }

}
