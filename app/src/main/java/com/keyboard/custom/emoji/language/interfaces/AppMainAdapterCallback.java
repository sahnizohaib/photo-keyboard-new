package com.keyboard.custom.emoji.language.interfaces;

public interface AppMainAdapterCallback {
    void itemClicked(int position);
}
