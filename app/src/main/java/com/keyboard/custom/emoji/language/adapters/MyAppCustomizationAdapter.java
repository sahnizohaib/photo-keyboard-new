package com.keyboard.custom.emoji.language.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.keyboard.custom.emoji.language.R;
import com.keyboard.custom.emoji.language.interfaces.CustomTypeAdapterCallback;
import com.keyboard.custom.emoji.language.models.CustomTypeModel;
import com.keyboard.custom.emoji.language.utils.MyAdsHelper;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.ArrayList;

public class MyAppCustomizationAdapter extends RecyclerView.Adapter<MyAppCustomizationAdapter.MyViewHolder> {

    private ArrayList<CustomTypeModel> list;
    private Context context;
    private CustomTypeAdapterCallback callback;

    public MyAppCustomizationAdapter(ArrayList<CustomTypeModel> list, Context context, CustomTypeAdapterCallback callback) {
        this.list = list;
        this.context = context;
        this.callback = callback;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_item_my_custom_rv, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        final CustomTypeModel model = list.get(position);
        //Log.d("onBindViewNew: ", "" + model.getThemeBackground());
        holder.tvCount.setText((position + 1) + "");
        holder.imageView.setImageResource(model.getThemeBackground());
        holder.tvThemeName.setText(model.getThemeName());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callback.onThemeClicked(model, position);
            }
        });
        try {
            MyAdsHelper.setScaleAnimation(holder.itemView);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        RoundedImageView imageView;
        TextView tvThemeName;
        TextView tvCount;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imageView);
            tvThemeName = itemView.findViewById(R.id.tvThemeName);
            tvCount = itemView.findViewById(R.id.tvCount);
        }
    }
}
