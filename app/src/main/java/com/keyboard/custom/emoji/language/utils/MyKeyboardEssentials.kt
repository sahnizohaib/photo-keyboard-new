package com.keyboard.custom.emoji.language.utils

import android.app.Activity
import android.content.Intent
import com.keyboard.custom.emoji.language.R


class MyKeyboardEssentials {
    companion object {

        fun shareToFriend(mContext: Activity) {
            try {
                val shareAppIntent = Intent(Intent.ACTION_SEND)
                shareAppIntent.type = "text/plain"
                val shareSub = "Hey..Kindly check this Application on Google Play!"
                val shareBody = mContext.getString(R.string.share_app)
                shareAppIntent.putExtra(Intent.EXTRA_SUBJECT, shareSub)
                shareAppIntent.putExtra(Intent.EXTRA_TEXT, shareBody)
                mContext.startActivity(Intent.createChooser(shareAppIntent, "Share App "))
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }
}