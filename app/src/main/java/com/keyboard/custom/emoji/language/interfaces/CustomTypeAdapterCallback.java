package com.keyboard.custom.emoji.language.interfaces;


import com.keyboard.custom.emoji.language.models.CustomTypeModel;

public interface CustomTypeAdapterCallback {
    void onThemeClicked(CustomTypeModel model, int position);
}
