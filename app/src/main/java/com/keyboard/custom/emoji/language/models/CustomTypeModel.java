package com.keyboard.custom.emoji.language.models;

public class CustomTypeModel {
    private String themeName;
    private Integer themeBackground;

    public CustomTypeModel(String themeName, Integer themeBackground) {
        this.themeName = themeName;
        this.themeBackground = themeBackground;
    }

    public String getThemeName() {
        return themeName;
    }

    public void setThemeName(String themeName) {
        this.themeName = themeName;
    }

    public Integer getThemeBackground() {
        return themeBackground;
    }

    public void setThemeBackground(Integer themeBackground) {
        this.themeBackground = themeBackground;
    }
}
