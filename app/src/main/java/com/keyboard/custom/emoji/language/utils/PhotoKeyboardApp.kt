package com.keyboard.custom.emoji.language.utils


import android.app.Application

import com.keyboard.custom.emoji.language.R
import com.google.android.gms.ads.MobileAds

class PhotoKeyboardApp : Application() {

    override fun onCreate() {
        super.onCreate()
        MobileAds.initialize(this, getString(R.string.appid_admob))
        FontsOverride.setDefaultFont(this, "DEFAULT", "fonts/quick_new.otf")
        FontsOverride.setDefaultFont(this, "MONOSPACE", "fonts/quick_new.otf")
    }

}
