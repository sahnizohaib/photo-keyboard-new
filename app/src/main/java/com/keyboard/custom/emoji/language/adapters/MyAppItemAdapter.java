package com.keyboard.custom.emoji.language.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.keyboard.custom.emoji.language.R;
import com.keyboard.custom.emoji.language.interfaces.AppMainAdapterCallback;
import com.keyboard.custom.emoji.language.models.MyAppModel;

import java.util.ArrayList;

public class MyAppItemAdapter extends RecyclerView.Adapter<MyAppItemAdapter.MyViewHolder> {

    private ArrayList<MyAppModel> list;
    private Context mContext;
    private AppMainAdapterCallback callback;

    public MyAppItemAdapter(ArrayList<MyAppModel> list, Context mContext, AppMainAdapterCallback callback) {
        this.list = list;
        this.mContext = mContext;
        this.callback = callback;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_item_my_app_rv, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, final int i) {
        MyAppModel model = list.get(i);
        Glide.with(mContext).load(model.getAppItemIcon()).into(myViewHolder.imageView);
        myViewHolder.textView.setText(model.getAppItemName());
        myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callback.itemClicked(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView textView;
        ImageView imageView2;
        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imageView);
            textView = itemView.findViewById(R.id.textView);
            imageView2 = itemView.findViewById(R.id.imageView2);
            YoYo.with(Techniques.Shake).duration(1400).repeat(YoYo.INFINITE).playOn(imageView2);
        }
    }
}
