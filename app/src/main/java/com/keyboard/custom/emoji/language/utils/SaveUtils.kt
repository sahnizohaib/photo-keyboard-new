package com.keyboard.custom.emoji.language.utils

import android.app.Activity
import android.graphics.Bitmap
import android.net.Uri
import android.os.Environment
import android.widget.Toast

import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException

object SaveUtils {


    fun saveBitmap(bitmap: Bitmap, activity: Activity): String {
        val uri: Uri? = null
        var files: File? = null
        var file1: File? = null
        try {
            if (Environment.getExternalStorageState() != Environment.MEDIA_MOUNTED) {
                Toast.makeText(activity, "No storage found", Toast.LENGTH_SHORT).show()
            } else {
                files = File(
                    Environment.getExternalStorageDirectory().toString()
                            + File.separator + ".Custom Keyboard App" + File.separator + "Keyboard Images" + File.separator
                )

                val success = files.mkdirs()
            }
            file1 = File(
                files,
                "CroppedImage " + System.currentTimeMillis() + ".PNG"
            )//when gallery empty then reset counter
            val output = FileOutputStream(file1!!)
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, output)
            output.flush()
            output.close()
            Toast.makeText(activity, "Saved", Toast.LENGTH_SHORT).show()

        } catch (e1: FileNotFoundException) {
            e1.printStackTrace()
        } catch (e1: IOException) {
            e1.printStackTrace()
        }

        return file1!!.absolutePath
    }
}
