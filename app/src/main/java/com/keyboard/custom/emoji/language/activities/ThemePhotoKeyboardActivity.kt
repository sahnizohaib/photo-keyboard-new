package com.keyboard.custom.emoji.language.activities

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.google.android.gms.ads.AdView
import com.keyboard.custom.emoji.language.R
import com.keyboard.custom.emoji.language.utils.MyAdsHelper
import com.keyboard.custom.emoji.language.utils.MyConstants
import com.keyboard.custom.emoji.language.utils.SaveUtils
import com.theartofdev.edmodo.cropper.CropImageView
import kotlinx.android.synthetic.main.activity_gallery_customization.*
import java.io.File


class ThemePhotoKeyboardActivity : AppCompatActivity(), View.OnClickListener {

    private var myBgImageUrl: String? = null
    private lateinit var sharedPref: SharedPreferences
    private lateinit var cropImageView: CropImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gallery_customization)
        initViews()
    }

    private fun initViews() {
        val mAdView = findViewById<AdView>(R.id.adView)
        MyAdsHelper.loadBannerAd(mAdView, this)
        sharedPref =
            applicationContext.getSharedPreferences(MyConstants.PREFS_NAME, Context.MODE_PRIVATE)
        cropImageView = findViewById(R.id.ivImage)
        cvChoose.setOnClickListener(this)
        cvSet.setOnClickListener(this)
        ivBack.setOnClickListener(this)

        cropImageView.setOnCropImageCompleteListener(object : CropImageView.OnCropImageCompleteListener {
            override fun onCropImageComplete(
                view: CropImageView?,
                result: CropImageView.CropResult?
            ) {

            }
        })
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.cvChoose -> {
                if (ActivityCompat.checkSelfPermission(
                        this,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                    ) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                        this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                    ) == PackageManager.PERMISSION_GRANTED
                ) {
                    try {
                        val intent = Intent()
                        intent.type = "image/*"
                        intent.action = Intent.ACTION_GET_CONTENT
                        startActivityForResult(Intent.createChooser(intent, "Select Picture"), 2)
                    } catch (e: Exception) {
                    }
                } else {
                    Toast.makeText(
                        this,
                        "Please grant Storage permission first.",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
            R.id.cvSet -> {
                try {
                    if (myBgImageUrl != null) {
                        val bitmapCropped = cropImageView.croppedImage
                        val bgPathNew = SaveUtils.saveBitmap(bitmapCropped, this)
                        if (bgPathNew != null) {
                            sharedPref.edit()
                                .putString(MyConstants.THEME_KEYBOARD_GALLERY, bgPathNew)
                                .apply()
                            sharedPref.edit().putString(
                                MyConstants.THEME_KEYBOARD_MODE,
                                MyConstants.THEME_KEYBOARD_MODE_GALLERY
                            )
                                .apply()

                            Toast.makeText(
                                this,
                                "$bgPathNew is set as Keyboard Background.",
                                Toast.LENGTH_LONG
                            ).show()
                            onBackPressed()
                        }
                    } else {
                        Toast.makeText(this, "Cannot Set this Image.", Toast.LENGTH_LONG).show()
                    }
                } catch (e: Exception) {
                }
            }
            R.id.ivBack -> {
                onBackPressed()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 2 && data != null) {
            if (resultCode == Activity.RESULT_OK) {
                try {
                    val imageUri = data.data
                    myBgImageUrl = getPAthFromUri(this, imageUri!!)
                    val bitmap = MediaStore.Images.Media.getBitmap(
                        this.contentResolver,
                        Uri.fromFile(
                            File(
                                myBgImageUrl!!
                            )
                        )
                    )
                    val resizedBitmap = Bitmap.createScaledBitmap(
                        bitmap, cvImage.width, cvImage.height, false
                    )
                    cropImageView.setImageBitmap(resizedBitmap)
                } catch (e: Exception) {
                }
            } else {
                Toast.makeText(this, "Cannot Get Image", Toast.LENGTH_LONG).show()
            }

        }
    }

    @SuppressLint("NewApi")
    fun getPAthFromUri(context: Context, uri: Uri): String {
        var filePath = ""
        val wholeID = DocumentsContract.getDocumentId(uri)
        try {
            val id = wholeID.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[1]
            val column = arrayOf(MediaStore.Images.Media.DATA)
            val sel = MediaStore.Images.Media._ID + "=?"

            val cursor = context.getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                column, sel, arrayOf(id), null
            )
            val columnIndex = cursor!!.getColumnIndex(column[0])
            if (cursor.moveToFirst()) {
                filePath = cursor.getString(columnIndex)
            }
            cursor.close()
        } catch (e: Exception) {
        }
        return filePath
    }

}
