package com.keyboard.custom.emoji.language.activities

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.Settings
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.InterstitialAd
import com.keyboard.custom.emoji.language.R
import com.keyboard.custom.emoji.language.adapters.MyAppItemAdapter
import com.keyboard.custom.emoji.language.dialogs.NewSettingsDialog
import com.keyboard.custom.emoji.language.interfaces.AppMainAdapterCallback
import com.keyboard.custom.emoji.language.models.MyAppModel
import com.keyboard.custom.emoji.language.utils.MyAdsHelper
import com.keyboard.custom.emoji.language.utils.MyKeyboardEssentials
import kotlinx.android.synthetic.main.activity_main.*

class MyAppMainActivity : AppCompatActivity() {

    private var listMain = ArrayList<MyAppModel>()
    private lateinit var adMobInterstitialAd: InterstitialAd

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initViews()
    }

    private fun initViews() {
        val mAdView = findViewById<AdView>(R.id.adView)
        MyAdsHelper.loadBannerAd(mAdView, this)
        adMobInterstitialAd = MyAdsHelper.loadInterstitialAd(this)
        val layoutManager = LinearLayoutManager(this)
        updateList()
        val adapter = MyAppItemAdapter(listMain, this, object :
            AppMainAdapterCallback {
            override fun itemClicked(position: Int) {
                when (position) {
                    0 -> {
                        gotoInputMethodActivity()
                    }
                    1 -> {
                        setCustomKeyboard()
                    }
                    2 -> {
                        val intent =
                            Intent(this@MyAppMainActivity, ThemeCustomizedActivity::class.java)
                        MyAdsHelper.adBeforeClick(
                            this@MyAppMainActivity,
                            adMobInterstitialAd,
                            intent
                        )
                    }
                    3 -> {
                        val intent =
                            Intent(this@MyAppMainActivity, ThemeColoredActivity::class.java)
                        MyAdsHelper.adBeforeClick(
                            this@MyAppMainActivity,
                            adMobInterstitialAd,
                            intent
                        )
                    }
                    4 -> {
                        val intent =
                            Intent(this@MyAppMainActivity, ThemePhotoKeyboardActivity::class.java)
                        MyAdsHelper.adBeforeClick(
                            this@MyAppMainActivity,
                            adMobInterstitialAd,
                            intent
                        )
                    }
                    5 -> {
                        val dialog = NewSettingsDialog(this@MyAppMainActivity)
                        dialog.show()
                    }
                    6 -> {
                        MyKeyboardEssentials.shareToFriend(this@MyAppMainActivity)
                    }
                }
            }
        })

        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = adapter
    }

    private fun setCustomKeyboard() {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showInputMethodPicker()
    }

    private fun gotoInputMethodActivity() {
        val intent = Intent(Settings.ACTION_INPUT_METHOD_SETTINGS)
        startActivity(intent)
    }

    private fun updateList() {
        listMain.add(MyAppModel("Activate Keyboard", R.drawable.ic_enable_keyboard))
        listMain.add(MyAppModel("Set Keyboard", R.drawable.ic_set_kyboard))
        listMain.add(MyAppModel("Wallpaper Keyboard", R.drawable.ic_theme_icon))
        listMain.add(MyAppModel("Customized Keyboard", R.drawable.ic_color_bg))
        listMain.add(MyAppModel("Photo Keyboard", R.drawable.ic_camera_icon))
        listMain.add(MyAppModel("Keyboard Settings", R.drawable.ic_settings_main))
        listMain.add(MyAppModel("Share App", R.drawable.ic_share_app))
    }

    override fun onStart() {
        super.onStart()
        appPermissionsCheck()
    }

    private fun appPermissionsCheck() {
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.READ_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ),
                1
            )
        } else {
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 1) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Permissions are granted!", Toast.LENGTH_SHORT).show()
            }
        }
    }


}
