@file:Suppress("DEPRECATION")

package com.keyboard.custom.emoji.language.services

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.drawable.BitmapDrawable
import android.inputmethodservice.InputMethodService
import android.inputmethodservice.Keyboard
import android.inputmethodservice.KeyboardView
import android.media.AudioManager
import android.net.Uri
import android.os.Vibrator
import android.provider.MediaStore
import android.text.InputType
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.LinearLayout
import com.google.android.gms.ads.AdView
import com.keyboard.custom.emoji.language.R
import com.keyboard.custom.emoji.language.activities.ThemePhotoKeyboardActivity
import com.keyboard.custom.emoji.language.utils.MyAdsHelper
import com.keyboard.custom.emoji.language.utils.MyConstants
import java.io.File


@Suppress("DEPRECATION")
class PhotoKeyboardService : InputMethodService(), KeyboardView.OnKeyboardActionListener {

    private var myKeyboardView: KeyboardView? = null
    private var myEnglishKeyboard: Keyboard? = null
    private var symbolKeyboard: Keyboard? = null
    private var symbolShiftedKeyboard: Keyboard? = null
    private var myEmojiOneKeyboard: Keyboard? = null
    private var myEmojiTwoKeyboard: Keyboard? = null
    private var myEmojiThirdKeyboard: Keyboard? = null
    private var myEmojiFourthKeyboard: Keyboard? = null
    private var myEmojiFifthKeyboard: Keyboard? = null
    private var capsLockEnabled = false
    private var keyboardParentLayout: LinearLayout? = null
    private lateinit var modeReceived: String
    private var shouldVibrate = false
    private var shouldSound = false
    private var shouldKey = false
    private lateinit var currentKeyboard: Keyboard
    private var predictionOn = false

    companion object {
        private lateinit var sharedPref: SharedPreferences
        var isModeApp = true
        var selectedThemeIndex = 0
    }

    override fun onCreateInputView(): View {
        sharedPref =
            applicationContext.getSharedPreferences(MyConstants.PREFS_NAME, Context.MODE_PRIVATE)
        MyConstants.initLists()
        val root = layoutInflater.inflate(R.layout.keyboard, null)
        keyboardParentLayout = root.findViewById<LinearLayout>(R.id.mainView)
        val mAdView = root.findViewById<AdView>(R.id.adView)
        MyAdsHelper.loadBannerAd(mAdView, this)
        myKeyboardView = root.findViewById<View>(R.id.keyboard) as KeyboardView
        myEnglishKeyboard = Keyboard(this, R.xml.custom_english_keyboard)
        symbolKeyboard = Keyboard(this, R.xml.custom_symbols_keyboard)
        symbolShiftedKeyboard = Keyboard(this, R.xml.custom_symbols_shifted_keyboard)
        myEmojiOneKeyboard = Keyboard(this, R.xml.custom_emoji_keyboard_first)
        myEmojiTwoKeyboard = Keyboard(this, R.xml.custom_emoji_keyboard_second)
        myEmojiThirdKeyboard = Keyboard(this, R.xml.custom_emoji_keyboard_third)
        myEmojiFourthKeyboard = Keyboard(this, R.xml.custom_emoji_keyboard_four)
        myEmojiFifthKeyboard = Keyboard(this, R.xml.custom_emoji_keyboard_five)
        if (myEnglishKeyboard != null) {
            setMyKeyboard(myEnglishKeyboard!!)
        }
        myKeyboardView!!.setOnKeyboardActionListener(this)
        return root
    }

    override fun onCreate() {
        super.onCreate()
        //Log.d("KeyboardService ", ": onCreate")
    }

    override fun onStartInputView(info: EditorInfo?, restarting: Boolean) {
        super.onStartInputView(info, restarting)
        //Log.d("KeyboardService ", ": onStartInputView")
        val originalParent = myKeyboardView!!.parent as ViewGroup
        originalParent.setPadding(0, 0, 0, 0)
        myKeyboardView!!.setPopupParent(originalParent)
        selectedThemeIndex = sharedPref.getInt(MyConstants.THEME_KEYBOARD_INDEX, 0)
        //setInputView(onCreateInputView())
        modeReceived = sharedPref.getString(
            MyConstants.THEME_KEYBOARD_MODE,
            MyConstants.THEME_KEYBOARD_DRAWABLE
        )!!
        shouldVibrate = sharedPref.getBoolean(MyConstants.KEYBOARD_VIBRATE_KEY, false)
        shouldSound = sharedPref.getBoolean(MyConstants.KEYBOARD_SOUND_KEY, false)
        shouldKey = sharedPref.getBoolean(MyConstants.KEYBOARD_PREVIEW_KEY, false)
        myKeyboardView!!.isPreviewEnabled = shouldKey
        //Log.d("CLICKed inService", " " + PhotoKeyboardService.isModeApp)
        if (modeReceived == MyConstants.THEME_KEYBOARD_MODE_DRAWABLE) {
            try {
                val drawablePrefs =
                    sharedPref.getInt(MyConstants.THEME_KEYBOARD_DRAWABLE, R.color.colorBlack)
                if (keyboardParentLayout != null) {
                    keyboardParentLayout!!.setBackgroundResource(drawablePrefs)
                }
            } catch (e: Exception) {
            }
        } else if (modeReceived == MyConstants.THEME_KEYBOARD_MODE_GALLERY) {
            val pathPrefs = sharedPref.getString(MyConstants.THEME_KEYBOARD_GALLERY, "path")
            if (keyboardParentLayout != null) {
                try {
                    val bitmap = MediaStore.Images.Media.getBitmap(
                        this.contentResolver,
                        Uri.fromFile(
                            File(
                                pathPrefs!!
                            )
                        )
                    )
                    val dr = BitmapDrawable(bitmap)
                    keyboardParentLayout!!.setBackgroundDrawable(dr)
                } catch (e: Exception) {
                }
            }
        }

    }

    override fun onStartInput(attribute: EditorInfo?, restarting: Boolean) {
        super.onStartInput(attribute, restarting)
        predictionOn = false
        //Log.d("KeyboardService ", ": onStartInput")
        try {
            when (attribute!!.inputType and InputType.TYPE_MASK_CLASS) {
                InputType.TYPE_CLASS_TEXT -> {
                    predictionOn = true

                    val variation = attribute.inputType and InputType.TYPE_MASK_VARIATION

                    if (variation == InputType.TYPE_TEXT_VARIATION_PASSWORD || variation == InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD) {
                        predictionOn = false
                    }

                    if (variation == InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
                        || variation == InputType.TYPE_TEXT_VARIATION_URI
                        || variation == InputType.TYPE_TEXT_VARIATION_FILTER
                    ) {
                        predictionOn = false
                    }

                    if (attribute.inputType and InputType.TYPE_TEXT_FLAG_AUTO_COMPLETE != 0) {
                        predictionOn = false
                    }
                }
            }
        } catch (e: Exception) {
        }
    }

    override fun onBindInput() {
        super.onBindInput()
        //Log.d("KeyboardService ", ": onBind")
    }

    override fun onUnbindInput() {
        super.onUnbindInput()
        //Log.d("KeyboardService ", ": onUnBind")
    }

    private fun setMyKeyboard(keyboard: Keyboard) {
        try {
            currentKeyboard = keyboard
            myKeyboardView!!.keyboard = keyboard
        } catch (e: Exception) {
        }
    }

    private fun playClick(keyCode: Int) {
        try {
            val am = getSystemService(Context.AUDIO_SERVICE) as AudioManager
            val vb = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
            if (shouldVibrate) {
                vb.vibrate(40)
            }
            if (shouldSound) {
                when (keyCode) {
                    32 -> am.playSoundEffect(AudioManager.FX_KEYPRESS_SPACEBAR, 1f)
                    Keyboard.KEYCODE_DONE, 10 -> am.playSoundEffect(
                        AudioManager.FX_KEYPRESS_RETURN,
                        1f
                    )
                    Keyboard.KEYCODE_DELETE -> am.playSoundEffect(
                        AudioManager.FX_KEYPRESS_DELETE,
                        1f
                    )
                    else -> am.playSoundEffect(AudioManager.FX_KEYPRESS_STANDARD, 1f)
                }
            }
        } catch (e: Exception) {
        }
    }

    override fun onPress(i: Int) {

    }

    override fun onRelease(i: Int) {

    }

    override fun onKey(primaryCode: Int, keyCodes: IntArray) {
        //Log.d("KEYPRESSED", "p code: $primaryCode")
        val inputConnection = currentInputConnection
        playClick(primaryCode)
        when (primaryCode) {
            Keyboard.KEYCODE_DELETE -> {
                currentInputConnection.sendKeyEvent(
                    KeyEvent(
                        KeyEvent.ACTION_DOWN,
                        KeyEvent.KEYCODE_DEL
                    )
                )
                currentInputConnection.sendKeyEvent(
                    KeyEvent(
                        KeyEvent.ACTION_UP,
                        KeyEvent.KEYCODE_DEL
                    )
                )
            }
            Keyboard.KEYCODE_SHIFT -> {
                if (myKeyboardView == null) {
                    return
                }
                if (currentKeyboard == myEnglishKeyboard) {
                    capsLockEnabled = !capsLockEnabled
                    myEnglishKeyboard!!.isShifted = capsLockEnabled
                    myKeyboardView!!.invalidateAllKeys()
                } else if (currentKeyboard == myEmojiOneKeyboard) {
                    setMyKeyboard(myEmojiTwoKeyboard!!)
                } else if (currentKeyboard == myEmojiTwoKeyboard) {
                    setMyKeyboard(myEmojiThirdKeyboard!!)
                } else if (currentKeyboard == myEmojiThirdKeyboard) {
                    setMyKeyboard(myEmojiFourthKeyboard!!)
                } else if (currentKeyboard == myEmojiFourthKeyboard) {
                    setMyKeyboard(myEmojiFifthKeyboard!!)
                } else if (currentKeyboard == myEmojiFifthKeyboard) {
                    setMyKeyboard(myEnglishKeyboard!!)
                } else if (currentKeyboard == symbolKeyboard) {
                    setMyKeyboard(symbolShiftedKeyboard!!)
                } else if (currentKeyboard == symbolShiftedKeyboard) {
                    setMyKeyboard(symbolKeyboard!!)
                }


            }
            Keyboard.KEYCODE_DONE -> {
                inputConnection.sendKeyEvent(KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_ENTER))
                inputConnection.sendKeyEvent(KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_ENTER))
            }

            1001 -> {
                try {
                    requestHideSelf(0)
                } catch (e: Exception) {
                }
                //requestHideSelf(InputMethodManager.HIDE_IMPLICIT_ONLY);
            }
            1010 -> {
                if (currentKeyboard == myEmojiOneKeyboard || currentKeyboard == myEmojiTwoKeyboard || currentKeyboard == myEmojiThirdKeyboard || currentKeyboard == myEmojiFourthKeyboard
                ) {
                    setMyKeyboard(myEnglishKeyboard!!)
                } else {
                    setMyKeyboard(myEmojiOneKeyboard!!)
                }
            }
            1012 -> {
                selectedThemeIndex++
                //Log.d("SELECTEDINDEX", " Service " + selectedThemeIndex)
                if (selectedThemeIndex >= MyConstants.list.size) {
                    selectedThemeIndex = 0
                }
                sharedPref.edit()
                    .putInt(
                        MyConstants.THEME_KEYBOARD_INDEX,
                        PhotoKeyboardService.selectedThemeIndex
                    ).apply()
                if (keyboardParentLayout != null) {
                    keyboardParentLayout!!.setBackgroundResource(MyConstants.list[selectedThemeIndex].themeBackground)
                    sharedPref.edit()
                        .putInt(
                            MyConstants.THEME_KEYBOARD_DRAWABLE,
                            MyConstants.list[selectedThemeIndex].themeBackground
                        )
                        .apply()
                    sharedPref.edit().putString(
                        MyConstants.THEME_KEYBOARD_MODE,
                        MyConstants.THEME_KEYBOARD_MODE_DRAWABLE
                    ).apply()
                }
            }
            1013 -> {
                val intent =
                    Intent(this@PhotoKeyboardService, ThemePhotoKeyboardActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
            }

            1015 -> {
                if (currentKeyboard == myEnglishKeyboard) {
                    setMyKeyboard(symbolKeyboard!!)
                } else if (currentKeyboard == symbolKeyboard) {
                    setMyKeyboard(myEnglishKeyboard!!)
                } else if (currentKeyboard == symbolShiftedKeyboard) {
                    setMyKeyboard(myEnglishKeyboard!!)
                }
            }

            else -> {
                var code = primaryCode.toChar()
                //Log.d("KEYPRESSED", "code: $code")
                if (currentKeyboard == myEmojiOneKeyboard || currentKeyboard == myEmojiTwoKeyboard || currentKeyboard == myEmojiThirdKeyboard || currentKeyboard == myEmojiFourthKeyboard || currentKeyboard == myEmojiFifthKeyboard
                ) {
                    inputConnection.commitText(String(Character.toChars(primaryCode)), 1)
                } else {
                    if (Character.isLetter(code) && capsLockEnabled) {
                        code = Character.toUpperCase(code)
                    }
                    inputConnection.commitText(code.toString(), 1)
                }
            }
        }
    }

    override fun onText(charSequence: CharSequence) {

    }

    override fun swipeLeft() {

    }

    override fun swipeRight() {

    }

    override fun swipeDown() {

    }

    override fun swipeUp() {

    }

    override fun onFinishInput() {
        super.onFinishInput()
        try {
            if (myKeyboardView != null) {
                myKeyboardView!!.closing()
            }
        } catch (e: Exception) {
        }
    }
}
