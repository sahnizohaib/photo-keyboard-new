package com.keyboard.custom.emoji.language.activities

import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.ads.AdView
import com.keyboard.custom.emoji.language.R
import com.keyboard.custom.emoji.language.adapters.MyAppCustomizationAdapter
import com.keyboard.custom.emoji.language.interfaces.CustomTypeAdapterCallback
import com.keyboard.custom.emoji.language.models.CustomTypeModel
import com.keyboard.custom.emoji.language.services.PhotoKeyboardService
import com.keyboard.custom.emoji.language.utils.MyAdsHelper
import com.keyboard.custom.emoji.language.utils.MyConstants
import kotlinx.android.synthetic.main.activity_colored_customization.*

class ThemeCustomizedActivity : AppCompatActivity() {

    private var list = ArrayList<CustomTypeModel>()
    private lateinit var sharedPreferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_wallpaper_customization)
        initViews()
    }

    private fun initViews() {
        val mAdView = findViewById<AdView>(R.id.adView)
        MyAdsHelper.loadBannerAd(mAdView, this)
        sharedPreferences =
            applicationContext.getSharedPreferences(MyConstants.PREFS_NAME, Context.MODE_PRIVATE)
        initLists()
        ivBack.setOnClickListener { onBackPressed() }
        recyclerView.layoutManager = LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false)
        val adapter = MyAppCustomizationAdapter(
            list,
            this,
            object : CustomTypeAdapterCallback {
                override fun onThemeClicked(model: CustomTypeModel?, position: Int) {
                    PhotoKeyboardService.selectedThemeIndex = position
                    Log.d("SELECTEDINDEX", " Custom " + PhotoKeyboardService.selectedThemeIndex)
                    try {
                        Toast.makeText(
                            this@ThemeCustomizedActivity,
                            "" + model!!.themeName + " is selected as keyboard Background",
                            Toast.LENGTH_LONG
                        ).show()
                        sharedPreferences.edit()
                            .putInt(MyConstants.THEME_KEYBOARD_DRAWABLE, model!!.themeBackground)
                            .apply()
                        sharedPreferences.edit()
                            .putInt(
                                MyConstants.THEME_KEYBOARD_INDEX,
                                PhotoKeyboardService.selectedThemeIndex
                            )
                            .apply()
                        sharedPreferences.edit().putString(
                            MyConstants.THEME_KEYBOARD_MODE,
                            MyConstants.THEME_KEYBOARD_MODE_DRAWABLE
                        )
                            .apply()
                        onBackPressed()
                    } catch (e: Exception) {
                    }
                }
            })
        recyclerView.adapter = adapter
    }

    fun initLists() {
        list.add(CustomTypeModel("Theme Sea view", R.drawable.bg1))
        list.add(CustomTypeModel("Theme Snow View", R.drawable.bg2))
        list.add(CustomTypeModel("Theme Blurred Fish", R.drawable.bg3))
        list.add(CustomTypeModel("Theme Yellow Fish", R.drawable.bg4))
        list.add(CustomTypeModel("Theme Gradient Orange", R.drawable.bg5))
        list.add(CustomTypeModel("Theme Colorful World", R.drawable.bg6))
        list.add(CustomTypeModel("Theme Sea Sight", R.drawable.bg7))
        list.add(CustomTypeModel("Theme Sunset", R.drawable.bg8))
        list.add(CustomTypeModel("Theme Pink Hearts", R.drawable.bg9))
        list.add(CustomTypeModel("Theme River Side", R.drawable.bg10))
        list.add(CustomTypeModel("Theme Shades", R.drawable.bg11))
        list.add(CustomTypeModel("Theme Blocks", R.drawable.bg12))
        list.add(CustomTypeModel("Theme Circular Lines", R.drawable.bg13))
        list.add(CustomTypeModel("Theme Drops", R.drawable.bg14))
        list.add(CustomTypeModel("Theme Stars", R.drawable.bg15))
        list.add(CustomTypeModel("Theme Shapes", R.drawable.bg16))
        list.add(CustomTypeModel("Theme Flowers", R.drawable.bg17))
        list.add(CustomTypeModel("Theme Hearts", R.drawable.bg18))
        list.add(CustomTypeModel("Theme Lights", R.drawable.bg19))
        list.add(CustomTypeModel("Theme Tiles", R.drawable.bg20))
        list.add(CustomTypeModel("Theme Mountains", R.drawable.bg21))
        list.add(CustomTypeModel("Theme Digital", R.drawable.bg22))
        list.add(CustomTypeModel("Theme Leaves", R.drawable.bg23))
        list.add(CustomTypeModel("Theme Mountain View", R.drawable.bg24))
        list.add(CustomTypeModel("Theme Tiger", R.drawable.bg25))
        list.add(CustomTypeModel("Theme Lion", R.drawable.bg26))
        list.add(CustomTypeModel("Theme Yellow Leaves", R.drawable.bg27))
        list.add(CustomTypeModel("Theme White Lion", R.drawable.bg28))
        list.add(CustomTypeModel("Theme Night Street", R.drawable.bg29))
        list.add(CustomTypeModel("Theme Plant View", R.drawable.bg30))
        list.add(CustomTypeModel("Theme Sand", R.drawable.bg31))
        list.add(CustomTypeModel("Theme Sunrise", R.drawable.bg32))
        list.add(CustomTypeModel("Theme Desert", R.drawable.bg33))
        list.add(CustomTypeModel("Theme Camels & Mountains", R.drawable.bg34))
        list.add(CustomTypeModel("Theme Mountain Side", R.drawable.bg35))
        list.add(CustomTypeModel("Theme Strawberry Plate", R.drawable.bg36))
        list.add(CustomTypeModel("Theme Fresh Strawberry", R.drawable.bg37))
        list.add(CustomTypeModel("Theme Fruits", R.drawable.bg38))
        list.add(CustomTypeModel("Theme Pear", R.drawable.bg39))
        list.add(CustomTypeModel("Theme Lemon", R.drawable.bg40))
        list.add(CustomTypeModel("Theme Black Cat", R.drawable.bg41))
        list.add(CustomTypeModel("Theme Sleeping Cat", R.drawable.bg42))
        list.add(CustomTypeModel("Theme Black Cat 2", R.drawable.bg44))
        list.add(CustomTypeModel("Theme Naughty Cat", R.drawable.bg44))
        list.add(CustomTypeModel("Theme Black Butterfly", R.drawable.bg45))
        list.add(CustomTypeModel("Theme Yellow ", R.drawable.bg46))
        list.add(CustomTypeModel("Theme Group of Butterfly", R.drawable.bg47))
        list.add(CustomTypeModel("Theme Bonfire", R.drawable.bg48))
        list.add(CustomTypeModel("Theme Fire", R.drawable.bg49))
        list.add(CustomTypeModel("Theme Black & White Cat", R.drawable.bg50))
    }
}
