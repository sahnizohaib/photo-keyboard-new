package com.keyboard.custom.emoji.language.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;

import com.keyboard.custom.emoji.language.R;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;


public class MyAdsHelper {

    public static InterstitialAd loadInterstitialAd(Activity activity) {
        PurchaseHelper billingHelper = new PurchaseHelper(activity);
        InterstitialAd mInterstitialAd = null;
        mInterstitialAd = new InterstitialAd(activity);
        mInterstitialAd.setAdUnitId(activity.getString(R.string.interstitial_admob));
        if (billingHelper.shouldGiveAds()) {
            requestNewInterstitial(mInterstitialAd);
        }
        return mInterstitialAd;
    }

    public static void loadBannerAd(final AdView mAdView, Context context) {
        PurchaseHelper billingHelper = new PurchaseHelper(context);
        if (billingHelper.shouldGiveAds()) {
            mAdView.loadAd(new AdRequest.Builder().build());
        }
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                mAdView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                super.onAdFailedToLoad(errorCode);
            }

            @Override
            public void onAdClosed() {
                super.onAdClosed();
            }
        });
    }


    public static InterstitialAd requestNewInterstitial(InterstitialAd mInterstitialAd) {
        AdRequest adRequest = new AdRequest.Builder().build();
        mInterstitialAd.loadAd(adRequest);
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(int errorCode) {
                super.onAdFailedToLoad(errorCode);
            }

            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
            }
        });
        return mInterstitialAd;
    }

    public static void adBeforeClick(final Activity context, final InterstitialAd mInterstitialAd, final Intent intent) {
        if (mInterstitialAd != null && mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
            mInterstitialAd.setAdListener(new AdListener() {
                public void onAdClosed() {
                    super.onAdClosed();
                    context.startActivity(intent);
                    try {
                        mInterstitialAd.loadAd(new AdRequest.Builder().build());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onAdFailedToLoad(int errorCode) {
                    super.onAdFailedToLoad(errorCode);

                }
            });
        } else {
            context.startActivity(intent);
        }
    }

    public static void setScaleAnimation(View view) {
        ScaleAnimation anim = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        anim.setDuration(500);
        view.startAnimation(anim);
    }

}
